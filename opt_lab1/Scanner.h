#ifndef OPT_LAB1_SCANNER_H
#define OPT_LAB1_SCANNER_H

#include "Structures.h"

class FileControl {

public:
    ifstream readStream;
    ofstream writeStream;

    FileControl(string, string);

    optional<char> readChar();

    void writeTokensToFile(optional<vector<Token>>);

    void writeSymbolToFile(Symbol);

    void writeStringToFile(string str);

    void writeTableToFile(map<int, string>);

};


class CharPositionControl {
    Position position;

public:
    void moveToNextRow();

    void moveWithStep(int);

    void move();

    Position getCurrentPosition();
};


class TableControl {

public:
    map<int, string> codeToLexemTable;
    map<string, int> lexemToCodeTable;

    TableControl() : codeToLexemTable(), lexemToCodeTable() {};

    bool isLexemAdded(string *);

    int getCodeByLexem(string *);

    void addLexemToStore(int, string);

    void print();
};


class SymbolCategories {
    const int ASCIIlenght = 127;
    int *asciiByCategoriesArr;

    void initCategories();

    //categories
    void initDigitCategory(int); //1
    void initLetterCategory(int); //2
    void initDelimiterCategory(int); //3
    void initMultiDelimiterCategory(int); //4
    void initCommentCategory(int); //5
    void initErrorCategory(int); //6
    void initSpaceSignCategory(int); //0

public:
    SymbolCategories() {
        asciiByCategoriesArr = new int[ASCIIlenght];
        initCategories();
    };

    int getCategoryBySymbol(int symbol) {
        return asciiByCategoriesArr[symbol];
    };
};


class CodeGenerator {
    int code;
public:
    CodeGenerator(int from = 0, int to = 0) : code(from) {};

    int formCode();
};


class Scanner {
    vector<Token> codes;

    TableControl *tableControl;
    FileControl *fileControl;
    CharPositionControl *charPositionControl;
    SymbolCategories *symbolCategories;

    Lexem createStringFromVector(vector<Symbol> *);

    optional<Symbol> scan();

    optional<Symbol> commentProcessor(vector<Symbol> *, vector<Symbol> *, Symbol *);

    map<int, CodeGenerator> initGenerators();

    map<int, int> generateEntityByCategoryMap();

    void initStore(map<int, CodeGenerator> *generatorByEntity);

    void insertLexemToTable(map<int, int> *, map<int, CodeGenerator> *, vector<Symbol> *);

    void printSymbols(vector<Symbol> *);

    void printSymbol(Symbol *);

public:
    Scanner(TableControl *, FileControl *, CharPositionControl *, SymbolCategories *);

    optional<vector<Token>> makeScanning();
};

#endif //OPT_LAB1_SCANNER_H



