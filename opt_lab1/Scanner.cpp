#include "Scanner.h"


FileControl::FileControl(string inputFilePath, string outputFilePath) : readStream(inputFilePath), writeStream(outputFilePath) {};

optional<char> FileControl::readChar() {
    if (this->readStream.eof()) {
        this->readStream.close();
        return nullopt;
    }

    char value = this->readStream.get();
    if (value == -1) {
        this->readStream.close();
        return nullopt;
    }
    return value;
}

void FileControl::writeTokensToFile(optional<vector<Token>> oTokens) {
    if (writeStream.is_open()) {
        writeStream << setw(10) << "Code" << " | " << setw(10) << "Row" << " | " << setw(10) << "Column"
                    << " | " << "\n";
        for (auto code: oTokens.value()) {
            writeStream << setw(10) << code.code << " | " << setw(10) << code.row << " | " << setw(10)
                        << code.column << " | " << "\n";
        }
    } else cout << "Unable to open file";
    writeStream.close();
}

void FileControl::writeSymbolToFile(Symbol symbol) {

    if (writeStream.is_open()) {
        writeStream << setw(10) << symbol.value << " | " << setw(10) << symbol.position.currentRow << " | "
                    << setw(10) << symbol.position.currentColumn<< " | " << setw(25) << symbol.note << " | " << "\n";
    } else cout << "Unable to open file";
}

void FileControl::writeTableToFile(map<int, string> codeToLexemTable) {

    if (writeStream.is_open()) {
        this->writeStringToFile("-------------------------------------------------------------");
        stringstream ss;
        ss << setw(10) << "Code" << " | " << setw(10) << "Lexem" << " | " << "";
        this->writeStringToFile(ss.str());
        for (auto it: codeToLexemTable) {
            writeStream << setw(10) << it.first << " | " << setw(10) << it.second << " | "
                        << "\n";
        }
        this->writeStringToFile("-------------------------------------------------------------");
        this->writeStringToFile("-------------------------------------------------------------");

    } else cout << "Unable to open file";


}

void FileControl::writeStringToFile(string str) {

    if (writeStream.is_open()) {
        writeStream << str << "\n";
    } else cout << "Unable to open file";
}

void CharPositionControl::move() {
    ++this->position.currentColumn;
}

void CharPositionControl::moveWithStep(int step) {
    this->position.currentColumn += step;
}

void CharPositionControl::moveToNextRow() {
    this->position.currentColumn = 0;
    ++this->position.currentRow;
}

Position CharPositionControl::getCurrentPosition() {
    return this->position;
}

bool TableControl::isLexemAdded(string *lexem) {
    return lexemToCodeTable.find(*lexem) != lexemToCodeTable.end();
}

int TableControl::getCodeByLexem(string *lexem) {
    return lexemToCodeTable[*lexem];
}

void TableControl::addLexemToStore(int code, string lexem) {
    if (!this->isLexemAdded(&lexem)) {
        codeToLexemTable.insert(pair<int, string>(code, lexem));
        lexemToCodeTable.insert(pair<string, int>(lexem, code));
    }
}

void TableControl::print() {
    //write to console
    cout << "-------------------------------------------------------------\n";
    cout << setiosflags(ios_base::right) << setw(10) << "Code" << " | " << setw(10) << "Lexem" << " | " << "\n";
    for (auto it: codeToLexemTable) {
        cout << setiosflags(ios_base::internal) << setw(10) << it.first << " | " << setw(10) << it.second << " | "
             << "\n";
    }
    cout << "-------------------------------------------------------------\n";
    cout << setiosflags(ios_base::right) << setw(10) << "Lexem" << " | " << setw(10) << "Code" << " | " << "\n";
    for (auto it: lexemToCodeTable) {
        cout << setiosflags(ios_base::internal) << setw(10) << it.first << " | " << setw(10) << it.second << " | "
             << "\n";
    }
    cout << "-------------------------------------------------------------\n";
    cout << "-------------------------------------------------------------\n";
}

void SymbolCategories::initCategories() {
    initErrorCategory(ctg::ERR);
    initDigitCategory(ctg::DIGIT);
    initLetterCategory(ctg::LETTER);
    initDelimiterCategory(ctg::DELIMITER);
    initMultiDelimiterCategory(ctg::MULTI_DELIMITER);
    initCommentCategory(ctg::COMMENT);
    initSpaceSignCategory(ctg::WHITESPACE);
}

void SymbolCategories::initErrorCategory(int categoryNumber) {
    for (int i = 0; i < ASCIIlenght; i++) {
        asciiByCategoriesArr[i] = categoryNumber;
    }
}

void SymbolCategories::initDigitCategory(int categoryNumber) {
    // 0..9 -> [48, ..., 57]
    const int from = 48;
    const int to = 57;

    for (int i = from; i <= to; i++) {
        asciiByCategoriesArr[i] = categoryNumber;
    }
}

void SymbolCategories::initLetterCategory(int categoryNumber) {
    const int from = 65;
    const int to = 90;

    for (int i = from; i <= to; i++) {
        asciiByCategoriesArr[i] = categoryNumber;
    }
}

void SymbolCategories::initCommentCategory(int categoryNumber) {
//    const int i = 42; // '*'
//    asciiByCategoriesArr[i] = categoryNumber;
}

void SymbolCategories::initDelimiterCategory(int categoryNumber) {
    const int dotSignNumber = 44; // ','
    const int semicolonNumber = 59; // ';'
    const int closeBracketNumber = 41; // ')'

    asciiByCategoriesArr[dotSignNumber] = categoryNumber;
    asciiByCategoriesArr[semicolonNumber] = categoryNumber;
    asciiByCategoriesArr[closeBracketNumber] = categoryNumber;
}

void SymbolCategories::initMultiDelimiterCategory(int categoryNumber) {
    const int colonSignNumber = 58; // ':'
    const int openBracket = 40; // '('
    const int closeBracket = 36; // '$'

    asciiByCategoriesArr[colonSignNumber] = categoryNumber;
    asciiByCategoriesArr[openBracket] = categoryNumber;
    asciiByCategoriesArr[closeBracket] = categoryNumber;
}

void SymbolCategories::initSpaceSignCategory(int categoryNumber) {
    const int spaceSignNumber = 32; // ' '
    const int carriageSignNumber = 13;
    const int newLineSignNumber = 10;
    const int horizontalTabSignNumber = 9;
    const int verticalTabSignNumber = 11;
    const int newPageSignNumber = 12;
    const int noname = -1;

//  const int signNumbers = { 32, 13, 10, 9, 11, };
    asciiByCategoriesArr[spaceSignNumber] = categoryNumber;
    asciiByCategoriesArr[carriageSignNumber] = categoryNumber;
    asciiByCategoriesArr[newLineSignNumber] = categoryNumber;
    asciiByCategoriesArr[horizontalTabSignNumber] = categoryNumber;
    asciiByCategoriesArr[verticalTabSignNumber] = categoryNumber;
    asciiByCategoriesArr[newPageSignNumber] = categoryNumber;
}

int CodeGenerator::formCode() {
    return code++;
}

map<int, int> Scanner::generateEntityByCategoryMap() {
    return map<int, int>{
            {ctg::LETTER,          ent::IDENTIFIER},
            {ctg::DIGIT,           ent::CONSTANT},
            {ctg::DELIMITER,       ent::DELIMITER},
            {ctg::MULTI_DELIMITER, ent::MULTI_DELIMITER}
    };
}

map<int, CodeGenerator> Scanner::initGenerators() {
    struct codeRange {
        int from;
        int to;
    };

    const codeRange delimiterRange = {0, 255};
    const codeRange multiDelimiterRange = {301, 400};
    const codeRange constantRange = {401, 500};
    const codeRange keyLexemsRange = {501, 600};
    const codeRange identifierRange = {601, 700};

    return map<int, CodeGenerator>{
            {ent::DELIMITER,       CodeGenerator(delimiterRange.from, delimiterRange.to)},
            {ent::MULTI_DELIMITER, CodeGenerator(multiDelimiterRange.from, multiDelimiterRange.to)},
            {ent::KEY_LEXEM,       CodeGenerator(keyLexemsRange.from, keyLexemsRange.to)},
            {ent::CONSTANT,        CodeGenerator(constantRange.from, constantRange.to)},
            {ent::IDENTIFIER,      CodeGenerator(identifierRange.from, identifierRange.to)}
    };
}

void Scanner::initStore(map<int, CodeGenerator> *generatorByEntity) {
    const map<int, vector<string>> reservedLexems = {
            {ent::DELIMITER,       {";",     ",",   "=",    "(",    ")",}},
            {ent::MULTI_DELIMITER, {":=",      "($",    "$)"}},
            {ent::KEY_LEXEM,       {"PROGRAM", "BEGIN", "END", "GOTO", "LINK", "IN", "OUT", "RETURN",}}
    };

    int code;
    for (auto entry : reservedLexems) {
        for (auto lexem : entry.second) {
            code = (*generatorByEntity)[entry.first].formCode();
            this->tableControl->addLexemToStore(code, lexem);
        }

    }
}

/////////////////////////////////////////////////
Scanner::Scanner(
        TableControl *tableControl,
        FileControl *fileControl,
        CharPositionControl *charPositionControl,
        SymbolCategories *symbolCategories
) {
    this->tableControl = tableControl;
    this->fileControl = fileControl;
    this->charPositionControl = charPositionControl;
    this->symbolCategories = symbolCategories;
}

optional<vector<Token>> Scanner::makeScanning() {
    map<int, int> entityByCategory = generateEntityByCategoryMap();
    map<int, CodeGenerator> generatorByEntity = this->initGenerators();
    this->initStore(&generatorByEntity);

    vector<Symbol> buffer;
    vector<Symbol> errBuffer;

    Symbol currentSymbol;

    optional<Symbol> oSymbol = scan();
    while (oSymbol) {
        currentSymbol = oSymbol.value();

        switch (currentSymbol.type) {
            case ctg::DIGIT: {
                int size = buffer.size();
                if (size) {
                    int prevSymbolType = buffer[size - 1].type;
                    if (prevSymbolType == ctg::MULTI_DELIMITER || prevSymbolType == ctg::DELIMITER) {
                        this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                    }
                }
                buffer.push_back(currentSymbol);
                oSymbol = this->scan();
                break;
            }
            case ctg::LETTER: {
                int size = buffer.size();
                if (size) {
                    int firstSymbolType = buffer[0].type;
                    int prevSymbolType = buffer[size - 1].type;
                    if (prevSymbolType == ctg::MULTI_DELIMITER || prevSymbolType == ctg::DELIMITER ||
                        firstSymbolType == ctg::DIGIT) {
                        this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                    }
                }
                buffer.push_back(currentSymbol);
                oSymbol = this->scan();
                break;
            }
            case ctg::MULTI_DELIMITER: {
                int size = buffer.size();

                if (size) {
                    this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                }

                if (currentSymbol.value == ':') { // :=
                    oSymbol = this->scan();

                    if (oSymbol) {
                        Symbol nextSymbol = oSymbol.value();
                        if (nextSymbol.value == '=') {
                            buffer.push_back(currentSymbol);
                            buffer.push_back(nextSymbol);
                            this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                        } else {
                            currentSymbol.type = ctg::DELIMITER;
                            buffer.push_back(currentSymbol);
                            this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                            break;
                        }
                    } else {
                        currentSymbol.type = ctg::DELIMITER;
                        buffer.push_back(currentSymbol);
                        this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                    }
                    oSymbol = this->scan();
                    break;
                }

                if (currentSymbol.value == '(') { // ($ , (*, (
                    oSymbol = this->scan();
                    if (oSymbol) {
                        Symbol nextSymbol = oSymbol.value();
                        if (nextSymbol.value == '$') { // ($
                            buffer.push_back(currentSymbol);
                            buffer.push_back(nextSymbol);
                            this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                        } else if (nextSymbol.value == '*') { // (*
                            currentSymbol.type = ctg::COMMENT;
                            nextSymbol.type = ctg::COMMENT;
                            buffer.push_back(currentSymbol);
                            buffer.push_back(nextSymbol);
                            oSymbol.emplace(nextSymbol);
                            break; //go to comment proccessor;
                        } else { // (
                            currentSymbol.type = ctg::DELIMITER;
                            buffer.push_back(currentSymbol);
                            this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                            break;
                        }
                    } else {
                        currentSymbol.type = ctg::DELIMITER;
                        buffer.push_back(currentSymbol);
                        this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                    }
                    oSymbol = this->scan();
                    break;
                }

                if (currentSymbol.value == '$') { // $), )
                    oSymbol = this->scan();
                    if (oSymbol) {
                        Symbol nextSymbol = oSymbol.value();
                        if (nextSymbol.value == ')') { // $)
                            buffer.push_back(currentSymbol);
                            buffer.push_back(nextSymbol);
                            this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                        } else { // $ as wrong symbol, ) as correct
                            errBuffer.push_back(currentSymbol);
                            buffer.push_back(nextSymbol);
                            this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                        }
                    } else {
                        errBuffer.push_back(currentSymbol);
                    }
                    oSymbol = this->scan();
                    break;
                }
                buffer.push_back(currentSymbol);
                oSymbol = this->scan();
                break;
            }
            case ctg::DELIMITER: {
                int size = buffer.size();
                if (size) {
                    this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                }
                buffer.push_back(currentSymbol);
                oSymbol = this->scan();
                break;
            }
            case ctg::WHITESPACE: {
                if (buffer.size() != 0) {
                    this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
                }

                if (currentSymbol.value == '\n') {
                    this->charPositionControl->moveToNextRow();
                } else {
                    this->charPositionControl->moveWithStep(int(buffer.size()));
                }
                oSymbol = this->scan();
                break;
            }
            case ctg::COMMENT: {
                oSymbol = this->commentProcessor(&buffer, &errBuffer, &currentSymbol);
                break;
            }
            case ctg::ERR: {
                errBuffer.push_back(currentSymbol);
                oSymbol = this->scan();
                break;
            }
            default: {

            }
        }
    }

    if (buffer.size()) {
        this->insertLexemToTable(&entityByCategory, &generatorByEntity, &buffer);
    }

    if (errBuffer.size()) {
        //write to console
        /*  cout << "ERRORS:" << "\n";
         cout << setiosflags(ios_base::right) << setw(10) << "Symbol" << " | " << setw(10) << "Row" << " | " << setw(10)
              << "Column" << " | " << "\n";*/
        //write to file
        this->fileControl->writeStringToFile("ERRORS:");
        stringstream ss;
        ss << setiosflags(ios_base::right) << setw(10) << "Symbol" << " | " << setw(10) << "Row" << " | " << setw(10)
           << "Column" << " | " << setw(25) << "Note" << " | " << "";
        this->fileControl->writeStringToFile(ss.str());

        this->printSymbols(&errBuffer);
        return nullopt;
    }

    //write to console
    // this->tableControl->print();
    //write to file
    this->fileControl->writeTableToFile(this->tableControl->codeToLexemTable);
    return this->codes;
}

optional<Symbol> Scanner::scan() {
    this->charPositionControl->move();

    optional<char> symbol = this->fileControl->readChar();
    if (!symbol) return nullopt;

    Position symbolPosition = this->charPositionControl->getCurrentPosition();

    char value = symbol.value();
    string note = "UNKNOWN SYMBOL";

    return Symbol{
            value,
            value,
            this->symbolCategories->getCategoryBySymbol(value),
            symbolPosition,
            note,
    };

}

void Scanner::insertLexemToTable(
        map<int, int> *entityByCategory,
        map<int, CodeGenerator> *generatorByEntity,
        vector<Symbol> *symbols
) {
    Lexem tempLexem = this->createStringFromVector(symbols);
    int firstSymbolCategory = (*symbols)[0].type;
    int code;
    if (this->tableControl->isLexemAdded(&tempLexem.lexem)) {
        code = this->tableControl->getCodeByLexem(&tempLexem.lexem);
    } else {
        int entity = (*entityByCategory)[firstSymbolCategory];
        code = (*generatorByEntity)[entity].formCode();
    }

    this->tableControl->addLexemToStore(code, tempLexem.lexem);
    this->codes.push_back(Token{
            code,
            tempLexem.position.currentRow,
            tempLexem.position.currentColumn
    });
    symbols->clear();
}

Lexem Scanner::createStringFromVector(vector<Symbol> *symbols) {
    string lexem;
    for (int i = 0; i < (*symbols).size(); i++) {
        lexem += (*symbols)[i].value;
    }
    return Lexem{
            lexem,
            (*symbols)[0].position
    };
}

optional<Symbol> Scanner::commentProcessor(vector<Symbol> *buffer, vector<Symbol> *errSymbols, Symbol *commentSymbol) {
    buffer->clear();

    optional<Symbol> oSymbol = scan();
    if (!oSymbol) {
        return oSymbol;
    }
    Symbol currentSymbol;

    while (oSymbol) {
        currentSymbol = oSymbol.value();
        if (currentSymbol.value == '*') {
            oSymbol = scan();
            if (oSymbol) {
                currentSymbol = oSymbol.value();
                if (currentSymbol.value == ')') {
                    return this->scan();
                }
            }
        } else {
            if (currentSymbol.value == '\n') {
                this->charPositionControl->moveToNextRow();
            }
            oSymbol = scan();
        }
    }
    char value = ' ';
    errSymbols->push_back(Symbol{
             value,
             -1,
             ctg::ERR,
             this->charPositionControl->getCurrentPosition(),
             "NOT CLOSED COMMENT(EOF)"

    }); // not closed comment
    return oSymbol;
}


void Scanner::printSymbol(Symbol *symbol) {
    //write to console
    /* std::cout << setiosflags(ios_base::internal) << setw(10) << symbol->value << " | " << setw(10)
               << symbol->position.currentRow << " | " << setw(10) << symbol->position.currentColumn << " | " << "\n";*/
    //write to file
    this->fileControl->writeSymbolToFile(*symbol);
}

void Scanner::printSymbols(vector<Symbol> *symbols) {
    for (auto symbol: *symbols) {
        this->printSymbol(&symbol);
    }
    this->fileControl->writeStream.close();
}