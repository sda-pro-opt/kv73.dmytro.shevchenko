#include <iostream>
#include <vector>

#include "Scanner.h"

using namespace std;

int startScan(string inputFilePath, string outputFilePath, string expFilePath, int numberOfTest) {


    auto *fileControl = new FileControl(inputFilePath, outputFilePath);
    auto *tableControl = new TableControl();
    auto *charPositionControl =  new CharPositionControl();
    auto *symbolCategories = new SymbolCategories();
    auto *scanner = new Scanner( tableControl, fileControl, charPositionControl, symbolCategories);

    optional<vector<Token>> oTokens = scanner->makeScanning();
    if (oTokens) {
        //write to file
        fileControl->writeTokensToFile(oTokens);
    };
    fileControl->writeStream.close();
    //write to console
    /* cout << setiosflags(ios_base::right) << setw(10) << "Code" << " | " << setw(10) << "Row" << " | " << setw(10)
          << "Column" << " | " << "\n";
     for (auto code: oTokens.value()) {
         cout << setiosflags(ios_base::internal) << setw(10) << code.code << " | " << setw(10) << code.row << " | "
              << setw(10) << code.column << " | " << "\n";
     }*/

    //test check
    ifstream outFile(outputFilePath), expFile(expFilePath), inFile(inputFilePath);
    if (!outFile || !expFile || !inFile) {
        cout << "FILE NOT FOUND - " << "TEST" << numberOfTest ;
        return -1;
    }
    string s1((istreambuf_iterator<char>(outFile)), istreambuf_iterator<char>());
    string s2((istreambuf_iterator<char>(expFile)), istreambuf_iterator<char>());

    cout << "TEST" << numberOfTest << ":" << boolalpha << (s1 == s2) << "\n";
    inFile.close();
    outFile.close();
    expFile.close();
    return 0;

}

int main() {

    string inputFilePath, outputFilePath, expFilePath;

    //write tests here
    inputFilePath = "../tests/test1/input.sig";
    outputFilePath = "../tests/test1/generated.txt";
    expFilePath = "../tests/test1/expected.txt";
    startScan(inputFilePath, outputFilePath, expFilePath, 1);

    inputFilePath = "../tests/test2/input.sig";
    outputFilePath = "../tests/test2/generated.txt";
    expFilePath = "../tests/test2/expected.txt";
    startScan(inputFilePath, outputFilePath, expFilePath, 2);

    inputFilePath = "../tests/test3/input.sig";
    outputFilePath = "../tests/test3/generated.txt";
    expFilePath = "../tests/test3/expected.txt";
    startScan(inputFilePath, outputFilePath, expFilePath, 3);

    inputFilePath = "../tests/test4/input.sig";
    outputFilePath = "../tests/test4/generated.txt";
    expFilePath = "../tests/test4/expected.txt";
    startScan(inputFilePath, outputFilePath, expFilePath, 4);

    inputFilePath = "../tests/test5/input.sig";
    outputFilePath = "../tests/test5/generated.txt";
    expFilePath = "../tests/test5/expected.txt";
    startScan(inputFilePath, outputFilePath, expFilePath, 5);

    return 0;
}
