#ifndef OPT_LAB1_STRUCTURES_H
#define OPT_LAB1_STRUCTURES_H

#include <iostream>
#include <map>
#include <vector>
#include <fstream>
#include <optional>
#include <string>
#include <iomanip>
#include <sstream>

using namespace std;

//Structures
struct Position {
    int currentRow = 1;////////////////////////
    int currentColumn = 0;
};

struct Token {
    int code;
    int row;
    int column;

    void set(int iCode, int iRow, int iColumn) {
        code = iCode;
        row = iRow;
        column = iColumn;
    }
};

struct Symbol {
    char value;
    int code;
    int type;
    Position position;
    string note;
};

struct Lexem {
    string lexem;
    Position position;
};

namespace ctg {
    enum Categories {
        WHITESPACE,
        DIGIT,
        LETTER,
        DELIMITER,
        MULTI_DELIMITER,
        COMMENT,
        ERR,
    };
}

namespace ent {
    enum Entities {
        DELIMITER,
        MULTI_DELIMITER,
        KEY_LEXEM,
        CONSTANT,
        IDENTIFIER,
    };
}

#endif //OPT_LAB1_STRUCTURES_H
